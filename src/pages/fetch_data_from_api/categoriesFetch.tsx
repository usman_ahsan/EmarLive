import React from 'react'
import items from '../../models/exhibitionItems';
import styles from '../../theme/MainPage.module.css'
import {  IonImg} from '@ionic/react';
import '../../theme/variables.css'
import health from '../../assets/icons/cream.png'
import fashion from '../../assets/icons/shop.png'
import electronic from '../../assets/icons/electronics.png'
import baby from '../../assets/icons/rubber-duck.png'
import { RouteComponentProps } from 'react-router';
export default class FetchDataFromApi extends React.Component<any, RouteComponentProps>{
     categories:items[] = this.props.itemList
     CategoryImages = [electronic, fashion, baby, health]
     itemNames= ["Electronics", "Fashion", "Baby products", "Health & Wellness"]
    
     render(){
        
         return(
         
           <div className={styles.card}>
              
            {this.categories.slice(0,4).map((item,i) => (
            
              <div key={i} className={styles.items} onClick={() => { this.props.history.push('/productsList')}} >
               
                  <IonImg src={this.CategoryImages[i]} style={{margin: 'auto', height:"6vh", width:'6vh'}}></IonImg>
                  
                  <p style={{textAlign:'center', fontSize:'1.7vh' , marginTop:'1vh',
                     color:'black', fontFamily:'RobotoMedium'}}>
                     {this.itemNames[i]}  
                  </p>
                  </div>
               
            ))
            }
            
           </div>
         
         )
     }  
}





   
