import React, { useState } from 'react'
import { RouteComponentProps } from 'react-router';
import { IonPage, IonHeader, IonContent, IonFooter, IonGrid, IonRow, IonCol, IonButtons, IonImg, IonFab, IonFabButton, IonAlert, IonList, IonItem, IonCard, IonCardContent, useIonViewWillEnter } from '@ionic/react';
import TitleBarComponentNoSearchBar from './title_bars/titleBarComponentNoSearchBar';
import chat from '../assets/icons/chat2.png'
import cart from '../assets/icons/cart2.png'
import deals from '../assets/icons/deal2.png'
import styles from '../theme/MainPage.module.css'
import back from '../assets/icons/arrow-left.svg'
import add from '../assets/icons/plus.svg'
import LiveDealsItem from '../models/liveDealsListItems';
import trash from '../assets/icons/trash-2.svg'



const LiveDeals : React.FC<RouteComponentProps> = (props) => {
    const [showAlert, setShowAlert] = useState(false);
    const [showItemDelete, setShowItemDelete] = useState(false);
    const liveDealsList : LiveDealsItem[] = []
    const [liveDataList, setLiveDataList] = useState(liveDealsList)
    
    
   const LoggedIn = () =>{
        return (
         <IonFooter className={styles.footerLoggedIn}>
     
         <IonGrid>
     
         <IonRow style={{ margin:0}} >
    
         <IonCol col-1 size="2">
         <IonButtons  >
         <IonImg style={{height: '7vh', width: '5vh', marginLeft: 'auto', marginRight:'auto' }} src={back} 
              onClick={e => props.history.replace('/MainPage')} />
         </IonButtons>
         </IonCol>
    
         <IonCol col-4 >
         <IonButtons  >
         <IonImg style={{height: '7vh', width: '7vh', marginLeft: 'auto', marginRight:'auto' }} src={chat} />
         </IonButtons>
         </IonCol>
     
         <IonCol col-4  >
        
         <IonButtons >
         <IonImg style={{height: '7vh', width: '15vw', marginLeft: 'auto', marginRight:'auto' }}
          src={deals}  />
         </IonButtons>
          
         </IonCol>
     
         <IonCol col-4  >
         <IonButtons >
         <IonImg style={{height: '7vh', width: '15vw', marginLeft: 'auto', marginRight:'auto' }} src={cart} />
         </IonButtons>
       
         </IonCol>
     
         </IonRow>
     
         </IonGrid>
           </IonFooter>
         );
       }
   const AlertDialog = () => {
       return (
        <IonAlert
        isOpen={showAlert}
        onDidDismiss={() => setShowAlert(false)}
        header={'Live Deals'}
        cssClass ="addAlertsStyle"
        inputs={[
          {
            name: 'productName',
            type: 'text',
            id: 'maximumPrice',
            placeholder: 'Enter Product Name'
          },
          {
            name: 'MaximumPrice',
            type: 'number',
            id: 'maximumPrice',
            placeholder: 'Enter Maximum Price'
          },
          {
            name: 'MinimumPrice',
            id: 'minimumPrice',
            type: 'number',
            placeholder: 'Enter Minimum Price'
          }
        ]}
        buttons={[
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Confirm Cancel');
            }
          },
          {
            text: 'Add',
            cssClass: 'positiveButton',
            handler: (data) => {
             
              setLiveDataList([...liveDataList, {
                productName: data.productName,
                maximumPrice: data.MaximumPrice,
                minimumPrice: data.MinimumPrice
              }])
             
            }
          }
        ]}
      />
       )
   }

   const AlertItemDelete = () => {
    return (
     <IonAlert
     isOpen={showItemDelete}
     onDidDismiss={() => setShowItemDelete(false)}
     header={'Delete Items'}
     backdropDismiss= {false}
     buttons={[
       {
         text: 'Cancel',
         role: 'cancel',
        
         handler: () => {
           console.log('Confirm Cancel');
         }
       },
       {
         text: 'Delete',
         handler: (data) => {
          
           setLiveDataList([...liveDataList, {
             productName: data.productName,
             maximumPrice: data.MaximumPrice,
             minimumPrice: data.MinimumPrice
           }])
         }
       }
     ]}
   />
    )
   }
   
   useIonViewWillEnter(() => {
      let arrayJson = localStorage.getItem('dataList');
      if(arrayJson !== null){
         let array: LiveDealsItem[] = JSON.parse(arrayJson)
         setLiveDataList(array)
      }
   })

   const LiveDealsListItems = () =>{
   
     return(
        <IonList style={{margin:0, padding: 0}}>
            {liveDataList.map((item, i) => (
                <IonItem key ={i}  style={{margin:0 , padding: 0}}>
                   <IonCard className={styles.liveDealsItem} >
                      <IonCardContent className={styles.liveDealsItem}>
                         <IonGrid style={{marging:0}} >
                           <IonRow >
                              <IonCol size="10">
                                 <p style={{fontFamily: 'RobotoMedium', fontSize:'2vh'}}>Product {item.productName} </p>
                              </IonCol>
                           </IonRow>
                           <IonRow >
                              <IonCol size="5">
                                 <p style={{fontFamily: 'RobotoLight' , fontSize:'1.6vh'}}>Max price ${item.maximumPrice} </p>
                              </IonCol>
                              <IonCol  size="5">
                              <p style={{fontFamily: 'RobotoLight' , fontSize:'1.6vh'}}>Min Price ${item.minimumPrice} </p>
                              </IonCol>
                              <IonCol>
                                 <img onClick={e => deleteitem(item.productName)} alt="Loading..."
                                  style={{height: '3vh', width: '3vh', marginTop: '-1vh'}} src={trash}  />
                              </IonCol>
                           </IonRow>
                         </IonGrid>
                      </IonCardContent>
                   </IonCard>
                   
                </IonItem>
            ))}
        </IonList> 
     )

   }

   const deleteitem = (name : String) => {
      setLiveDataList(liveDataList.filter(item => item.productName !== name) )
   }

   const saveDatainLocalStorage = () => {
    if(liveDataList.length > 0)
    {localStorage.setItem('dataList', JSON.stringify(liveDataList))}
   }

    return(
        <IonPage>
            <IonHeader>
                <TitleBarComponentNoSearchBar {...props} />
            </IonHeader>

            <IonContent>
              {LiveDealsListItems()}
              {saveDatainLocalStorage()}

                <AlertDialog />
                <AlertItemDelete />
                <IonFab vertical="bottom" horizontal="end" slot="fixed" >
                   <IonFabButton onClick={e => {setShowAlert(true)}}>
                      <IonImg src={add} />
                   </IonFabButton> 
                </IonFab>
            </IonContent>

            <div>
               <LoggedIn  />
            </div>           
        </IonPage>
    )


}

 

export default LiveDeals;