import { IonImg, IonRow, IonCol, IonGrid, IonAvatar, IonTitle, IonItemDivider} from '@ionic/react';
import React from 'react';
import { RouteComponentProps } from 'react-router';
import creditCard from '../../assets/icons/credit-card.png'
import purchaseHistory from '../../assets/icons/history.png'
import help from '../../assets/icons/information.png'
import settings from '../../assets/icons/settings.png'
import styless from '../../theme/sidemenu.module.css'
import styles from '../../theme/MainPage.module.css'
import FirebaseUser from '../../firebase_config/firebase_auth';
import Utils from '../../Utils';

let User : any
let userImage: any



const AuthenticatedUserSideMenu: React.FC<RouteComponentProps> = (props) => {


    if(FirebaseUser.User){
      User =FirebaseUser.User.displayName
      userImage = FirebaseUser.User.photoURL
      
    };




  return (
 
    <div className={styless.mainDiv}>
    <IonAvatar className={styles.emarsidelogo}>
    <IonImg   src={userImage} no-padding />
    </IonAvatar>

    <IonTitle style={{textAlign:"center", marginTop:2, marginBottom:1, fontFamily:'RobotoBold'}} >{User}</IonTitle>

    <IonItemDivider></IonItemDivider>

     <IonGrid>
         <IonRow>
             <IonCol size='2'>
                <IonImg style={{height: '3vh', width: '3vh'}} src={creditCard} />
             </IonCol>
             <IonCol>
                <h4 style={{fontFamily :"RobotoMedium",marginTop:'0.2vh', fontSize:'2.5vh'}}>{Utils.wallet}</h4>
             </IonCol>
         </IonRow>


         <IonRow>
             <IonCol size='2'>
                <IonImg style={{height: '3vh', width: '3vh'}} src={purchaseHistory} />
             </IonCol>
             <IonCol>
                <h4 style={{fontFamily :"RobotoMedium",marginTop:'0.2vh' , fontSize:'2.5vh'}}>{Utils.purchaseHistory}</h4>
             </IonCol>
         </IonRow>

        

         <IonRow>
             <IonCol size='2'>
                <IonImg style={{height: '3vh', width: '3vh'}} src={purchaseHistory} />
             </IonCol>
             <IonCol>
                <h4 style={{fontFamily :"RobotoMedium",marginTop:'0.2vh', fontSize:'2.5vh'}}>{Utils.recentView}</h4>
             </IonCol>
         </IonRow>

         <IonRow>
             <IonCol size='2'>
                <IonImg style={{height: '3vh', width: '3vh'}} src={help} />
             </IonCol>
             <IonCol>
                <h4 style={{fontFamily :"RobotoMedium",marginTop:'0.2vh', fontSize:'2.5vh'}}>{Utils.help}</h4>
             </IonCol>
         </IonRow>

         <IonRow>
             <IonCol size='2'>
                <IonImg style={{height: '3vh', width: '3vh'}} src={settings} />
             </IonCol>
             <IonCol>
                <h4 style={{fontFamily :"RobotoMedium",marginTop:'0.2vh', fontSize:'2.5vh'}}>{Utils.help}</h4>
             </IonCol>
         </IonRow>
     </IonGrid>
    </div>
        
     
  ); 
  };

 

   

  export default AuthenticatedUserSideMenu;