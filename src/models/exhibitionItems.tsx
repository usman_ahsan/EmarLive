import React from 'react'


interface items {
    
distance: String,
end_date: String,
event_timezone: String,
exhibition_price: String,
explanation: String,
image: string,
industry: String,
latitude: String,
longitude: String,
name: String,
organizer_name: "String",
overall_rating: Number,
payment_status: String,
place:String,
shortdes:String,
sno: String,
start_date: String,
total_reviews: Number,
type: String,
venu: String
  };

  export default items;