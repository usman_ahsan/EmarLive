
import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, isPlatform } from '@ionic/react';
import { IonReactRouter, IonReactHashRouter } from '@ionic/react-router';
import Home from './pages/Home';
import MainPage from './pages/Mainpage'
import AuthPage from '../src/pages/auth'
import fetchProduct from './pages/fetch_data_from_api/fetchProduct'

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import LiveDeals from './pages/liveDeals';
const Router : any = isPlatform('electron') ? IonReactHashRouter : IonReactRouter;

const App: React.FC = () => (

 

  <IonApp>
    <Router>
      <IonRouterOutlet>
        <Route path="/home" component={Home} exact={true} />
        <Route path="/Mainpage" component={MainPage} exact={true} />
        <Route path="/authpage" component={AuthPage} exact={true} />
        <Route path="/liveDeals" component={LiveDeals} exact={true} />
        <Route path="/productsList" component={fetchProduct} exact={true} />
        <Route exact path="/" render={() => <Redirect to="/home" />} />
      </IonRouterOutlet>
    </Router>
  </IonApp>
);

export default App;
