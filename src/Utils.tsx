import one from './assets/icons/one.png'
import two from './assets/icons/two.png'
import three from './assets/icons/three.png'
import four from './assets/icons/four.png'

export default class  Utils
 {    
    static productListApiUrl = "http://eamr.life/exhibition_webservice/getexhiuser.php"; 
    static Categories = "Categories"
    static recommendation = " Recommended for you"
    static footterMsg = "Sign in with social account or phonenumber "
    static loginButtonText = "Sign In"
    static sideMenuTitle = "Please SignIn"
    static productPageTitle = "Products"
    static wallet = "Wallet"
    static purchaseHistory = "Purchase History"
    static recentView = "Recent View"
    static help = "Help"
    static settings = "Settings"
    static imgArray = [one, two, three, four]
    
 }