import React from 'react'
import { IonImg, IonTitle, IonItemDivider} from '@ionic/react'
import styles from '../../theme/MainPage.module.css'
import logo from '../../assets/icons/download.png'
import styless from '../../theme/sidemenu.module.css'
import Utils from '../../Utils'

const SideMenu = () =>{
    
 return(

     <div className={styless.mainDiv}>

     <IonImg className={styles.emarsidelogo}  src={logo} no-padding />

     <IonTitle style={{textAlign:"center", marginTop:2, marginBottom:1}} >{Utils.sideMenuTitle}</IonTitle>

     <IonItemDivider></IonItemDivider>

     </div>
 );
}


export default SideMenu;