import {IonContent, IonPage, IonHeader, IonToolbar, IonRow, IonGrid, IonButtons, IonImg, IonItemDivider } from '@ionic/react';
import React from 'react';
import styles from '../theme/MainPage.module.css'
import firebase from 'firebase'
import { StyledFirebaseAuth } from 'react-firebaseui';
import home from '../assets/icons/home.png'
import {  RouteComponentProps } from 'react-router';
import logo from '../assets/icons/download.png'


const AuthPage: React.FC<RouteComponentProps> = (props) => {
  
   
   const uiConfig ={
    signInFlow: 'popup',
    signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        firebase.auth.PhoneAuthProvider.PROVIDER_ID
    ],
    callBacks: {
      signInSuccessWithAuthResult: () => props.history.goBack()
    }
}

const renderRedirect = () => {
    
    props.history.goBack()
 
}
    return (
       
    <IonPage>

      <IonHeader >
        <IonToolbar color="header" className={styles.header}>
          <IonGrid>
            <IonRow className={styles.row}>
              <IonButtons onClick={(e) => renderRedirect()} >
              <IonImg style={{height: 40, width: 40 , marginLeft: 10}} src={home} />
              </IonButtons>
            </IonRow>
          </IonGrid>
         </IonToolbar>
      </IonHeader> 

      <IonContent>
        
            <IonImg src ={logo} style={{height: '40vw', width: '40vw', marginLeft: 'auto', marginRight:'auto', marginTop:'2vh'}}></IonImg>
            <p style={{ fontSize:'2vh',fontWeight:'bold', textAlign:'center', color:'black' }}>Please Login into EMAR with easy login </p>
            <IonItemDivider></IonItemDivider>
            <div id='firebaseui-auth-container'>
                 <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
            </div> 
            <IonItemDivider></IonItemDivider>

          
        
          

      </IonContent>
    </IonPage>
    );
  
    
  };


  export default AuthPage;