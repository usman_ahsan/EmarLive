import React, {Component} from 'react'
import { IonCard, IonImg, IonCardContent, IonButtons, IonGrid, IonCol, IonRow, IonInfiniteScroll, IonInfiniteScrollContent} from '@ionic/react';
import heart from '../../assets/icons/heart.svg'
import DataList from '../../api_fetch_data/FetchProductList';
import Utils from '../../Utils';

class JustForYouListView extends Component<any>{
    isLoaded = false;
    state = { lastItem : 10}
    endItemCount = this.props.lastIndex

  categories: any;
  
    newItems(e: any) {

      let newValue = this.state.lastItem + 10
      if(newValue <= DataList.result.length )
         this.setState({
           lastItem: newValue
         });
         e.target.complete()
       }  
   
    
  componentDidMount() {

    this.setState({
                
      endItemCount: this.props.LastIndex
                    
                   
    });            
  }


render(){
    
  return(
          
    <div key={this.endItemCount} style={{margin: 'auto', justifyContent:'center', alignItems: 'center'}} 
   >
     
      {DataList.result.slice(0,this.state.lastItem).map((item,i) => (
         <IonCard  key={i} style={{padding:0}}>
            <IonCardContent style={{padding:0}}>
               <IonGrid style={{margin: 0, padding: 0}}>
                  <IonRow>
                     <IonCol sizeXl="6.5" sizeLg="5" sizeMd="6">
                     <IonImg src={( i <= Utils.imgArray.length) ? Utils.imgArray[i] : item.image} style={{height:"100%", width:'100%' }} no-padding></IonImg>
                    </IonCol> 

                     <IonCol sizeXl="5" sizeMd="6" style={{height: "15vh"}}>
                      <IonRow style={{height:"9vh"}} >  
                      <p style={{ fontSize: '1.8vh',width:"100%",
                        fontWeight:'bold' , color: 'black', fontFamily: 'RobotoMedium'}}>
                        {item.name}
                      </p>
                      <p style={{ fontSize: '1.4vh',width:"100%",
                        fontWeight:'bold' , color: 'black',fontFamily: 'RobotoLight'}}>
                        {item.industry}
                      </p>

                      </IonRow> 
                      <IonRow style={{  bottom: 0, marginTop:'1.8vh', marginLeft:'-0.5vh'}}>
                      <IonCol sizeXl="6.5" sizeLg="5" sizeMd="8" >
                        <p style={{ fontSize: '1.9vh',
                           fontWeight:'bold' , color: 'red',fontFamily: 'RobotoLight', marginTop:'1vh'}}>
                           $35.9
                        </p>
                      </IonCol>
                      <IonCol>
                        <IonButtons >
                          <IonImg src={heart} style={{height:'3vh' , width: '3vh',marginLeft:'auto'}}></IonImg>
                        </IonButtons>
                      </IonCol>
                     </IonRow> 

                    </IonCol> 
                   
                  </IonRow>
               </IonGrid>
            </IonCardContent>
          </IonCard>
            ))}
            
          <IonInfiniteScroll onIonInfinite={e => this.newItems(e)}>
           <IonInfiniteScrollContent loadingSpinner='bubbles' loadingText="Loading more">
           </IonInfiniteScrollContent>
         </IonInfiniteScroll>  
        
    </div>


          
         )
     }  
      
}

export default JustForYouListView;

   
