import {IonModal ,IonContent, IonImg, IonToolbar, IonButtons, IonRow, IonCol, IonGrid, useIonViewWillLeave} from '@ionic/react';
import React, {useState} from 'react';
import styles from '../../theme/MainPage.module.css'
import home from '../../assets/icons/home.png'
import menu from '../../assets/icons/list-menu.png'
import myEnterAnimation from '../../animations/enteranimation'
import myLeaveAnimation from '../../animations/exitAnimaton'
import SideMenu from '../side_menu/sidemenu'
import styless from '../../theme/sidemenu.module.css'
import arrow from '../../assets/icons/arrow-pointing-to-right.svg'
import { RouteComponentProps } from 'react-router';
import firebaseConfig from '../../firebase_config/ConfigFile';
import AuthenticatedUserSideMenu from '../side_menu/authentedSideMenu';


const TitleBarComponentNoSearchBar: React.FC<RouteComponentProps> = (props) => {
  const [showModal, setShowModal] = useState(false);
  const [isUserAuth , setUserauth] = useState(false);
  let titleName = props.location.state.title
  const renderRedirect = () => {
  props.history.push('/authpage')
  }


  const renderHomepage = () => {
    props.history.push('/Mainpage')
    }
  
  useIonViewWillLeave(() => {
  console.log('ionViewWillLeave event fired');
  setShowModal(false);
  });

  firebaseConfig.auth().onAuthStateChanged((user) => {
    if(user){
    //  User = user
      setUserauth(true)
    }});

    function LoginButton() {
        return (
          <IonButtons className={styless.socialbuttons} onClick={(e) => renderRedirect()}>
          Login in EMAR
         </IonButtons>
        )
    }

  return (
    <div>
        <IonToolbar color="header" className={styles.header}>
           <IonGrid>
             <IonRow className={styles.row}>
              
             <IonCol size="2" className={styles.item}>
                <IonButtons>
                <IonImg style={{height: 40, width: 40 , margin: 'auto'}} src={home} onClick={e => {renderHomepage()}} />
                </IonButtons>
            </IonCol>

          <IonCol col-9 size="8" >
            <h3 style={{fontFamily: 'RobotoMedium', color: 'white', textAlign: 'center', marginTop:'1.7vh'}}>{titleName}</h3>
          </IonCol>

          <IonCol size="2"  >
          <IonButtons onClick={() => setShowModal(true)} style={{ margin:'auto',height:50}}>
          <IonImg style={{height: 30, width: 30 , margin: 'auto'}} src={menu} />
          </IonButtons>
          </IonCol>

         </IonRow>

         </IonGrid>

        </IonToolbar>
        

        <IonContent> 
          <IonModal enterAnimation={myEnterAnimation} backdropDismiss={false}
                leaveAnimation={myLeaveAnimation} isOpen={showModal}>
            
            {(isUserAuth)? <AuthenticatedUserSideMenu {...props} /> : <SideMenu /> }
            {(isUserAuth)? "" : <LoginButton />}
            
           <IonButtons onClick={() => setShowModal(false)}>
           <IonImg style={{height: 35, width: 35 , marginBottom: 5, marginLeft: 5, padding: 2}} src={arrow} />
           </IonButtons>

          </IonModal>
        </IonContent>
        </div>
  ); 
  };

  

  export default TitleBarComponentNoSearchBar;
