import * as firebase from 'firebase/app';
import 'firebase/auth';

const firebaseConfig = firebase.initializeApp( {
    apiKey: "AIzaSyB2_3MOk2gYydjFJhXzVPGhuUCF2NmFjN0",
    authDomain: "myserver-17f67.firebaseapp.com",
    databaseURL: "https://myserver-17f67.firebaseio.com",
    projectId: "myserver-17f67",
    storageBucket: "myserver-17f67.appspot.com",
    messagingSenderId: "553167684253",
    appId: "1:553167684253:web:2cd76ad02d7ec088930a74"
  });
  
export default firebaseConfig;