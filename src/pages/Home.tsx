import { IonContent, IonPage, IonImg, IonTitle, useIonViewDidEnter, IonToast } from '@ionic/react';
import logo from '../assets/icons/download.png'
import React, { useState } from 'react';
import styles from '../theme/home.module.css'       
import { RouteComponentProps } from 'react-router';
import DataList from '../api_fetch_data/FetchProductList';
import FirebaseUser from '../firebase_config/firebase_auth';
import items from '../models/exhibitionItems';
import Utils from '../Utils'


const Home: React.FC<RouteComponentProps> = (props) => {
  const [showToast1, setShowToast1] = useState(false);
  let categories : items[]= [];
  const mystyle = {
  
    height: 300 
  };
  
  useIonViewDidEnter( () => {
     new FirebaseUser().getuser();
     //new DataList().dataList();
     getProductList()
    })

  const time = setTimeout(() => {
    
    setShowToast1(true)
    clearTimeout(time);
   
  }, 2500);
 
  const getProductList = () => {

    fetch(Utils.productListApiUrl)
    .then(res => res.json())
    .then(
      (result) => {
     
      categories = result.categorylistvalues
      DataList.result = categories;
      renderRedirect();
       
      },
      
      (error) => {
        console.log(error)
      }
    );

  }

  
 const renderRedirect = () => {
    
       props.history.replace('/MainPage')
    
  }

  const Toast = () => {
    return (<IonToast
      isOpen={showToast1}
      onDidDismiss={() => setShowToast1(false)}
      message="Your are on a slower internet connection. Please wait.."
      duration={1000}
    />)
  }
  
  return (
    
    <IonPage>

      <IonContent>  
         <div className={styles.centerContainer}>
         <IonImg style={mystyle} src={logo} />
         <IonTitle style={{textAlign: "center"}} ><b>Exhibition At My<br />Room</b></IonTitle>
         </div>
      </IonContent>
      <div>
        <Toast />
      </div>
    </IonPage>   
  );
};



export default Home;
