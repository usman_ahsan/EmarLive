import React, {Component} from 'react'
import styles from '../../theme/MainPage.module.css'
import { IonCard, IonImg, IonCardContent, IonButtons, IonGrid, IonCol, IonRow, IonInfiniteScroll, IonInfiniteScrollContent} from '@ionic/react';
import heart from '../../assets/icons/heart.svg'
import DataList from '../../api_fetch_data/FetchProductList';
import Utils from '../../Utils';
import banner from '../../assets/icons/banner.png'

class JustForYou extends Component<any>{
  isLoaded = false;
  state = { lastItem : 10}
  
  newItems(e: any) {
    let newValue = this.state.lastItem + 10
    if(newValue <= DataList.result.length )
         this.setState({
           lastItem: newValue
         });
         e.target.complete()
       }  
   

  componentDidMount() {
         

                 this.setState({
                    endItemCount: this.props.LastIndex 
                  });

            
            
            }


  render(){
    console.log(Utils.imgArray.length)
    return(
          
      <div>
        <IonGrid >
          <IonRow >
          {DataList.result.slice(0,this.state.lastItem).map((item,i) => (
            
            <IonCol sizeLg="3" sizeMd="4" sizeXl="3" key={i} size="6" style={{ padding:5, margin:0}} >
            <IonCard  className={styles.justForYou}>
            <IonCardContent style={{padding: 0, margin:0}}>
               <IonGrid style={{margin:0, padding:0}}>
                  <IonRow style={{width: '100%'}}>
                  <IonImg src={( i <= Utils.imgArray.length) ? Utils.imgArray[i] : item.image } alt={banner} style={{height:"20%", width:'100%'}}></IonImg>
                  </IonRow>

                  <IonRow style={{height: '10vh', margin:'0.9vh'}}>
                      <p style={{ fontSize: '1.7vh', width: '100%',
                        fontWeight:'bold' , color: 'black', fontFamily: 'RobotoMedium'}}>
                        {item.name}
                      </p>
                      <p  style={{ fontSize: '1.5vh',
                        fontWeight:'bold' , color: 'black',fontFamily: 'RobotoLight'}}>
                        {item.industry}
                      </p>
                  </IonRow>

                      
                  <IonRow style={{  bottom: 0, marginTop:'1vh',  marginLeft:'0.5vh', width: '100%'}}>
                      <IonCol sizeXl="10" sizeMd="9" sizeSm="12"  sizeXs="9">
                        <p style={{ fontSize: '1.7vh',
                           fontWeight:'bold' , color: 'red',fontFamily: 'RobotoLight'}}>
                           $35.9
                        </p>
                      </IonCol>
                      <IonCol>
                        <IonButtons >
                          <IonImg src={heart} style={{height:'3vh' , width: '3vh',justifyContent:'flex-end'}}></IonImg>
                        </IonButtons>
                      </IonCol>
                  </IonRow>  
                 

               </IonGrid>
            </IonCardContent>
          </IonCard>
          </IonCol>
            ))}
          </IonRow>
        </IonGrid> 

         <IonInfiniteScroll onIonInfinite={e => this.newItems(e)}>
           <IonInfiniteScrollContent loadingSpinner='bubbles' loadingText="Loading more">
           </IonInfiniteScrollContent>
         </IonInfiniteScroll>   
    </div>
          
         )
     }
     
      
}



export default JustForYou;

   
