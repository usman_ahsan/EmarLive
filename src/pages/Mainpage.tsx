import {IonLoading ,IonContent, IonPage, IonFooter, IonHeader, IonImg, IonButtons, IonRow, IonCol, IonGrid, useIonViewDidEnter, useIonViewWillEnter} from '@ionic/react';
import React, {useState} from 'react';
import styles from '../theme/MainPage.module.css'
import chat from '../assets/icons/chat2.png'
import cart from '../assets/icons/cart2.png'
import deals from '../assets/icons/deal2.png'
import listView from '../assets/icons/list-interface-symbol.png'
import gridView from '../assets/icons/squares.png'
import UserPicAlt from '../assets/icons/banner.png'
import { RouteComponentProps } from 'react-router';
import FetchDataFromApi from '../pages/fetch_data_from_api/categoriesFetch'
import JustForYou from './fetch_data_from_api/justForYou';
import JustForYouListView from './fetch_data_from_api/justForYouListView';
import TitleBarComponent from './title_bars/titleBarComponent';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import DataList from '../api_fetch_data/FetchProductList'
import FirebaseUser from '../firebase_config/firebase_auth';
import Utils from '../Utils';


let foot : any;
let categoryList : any;
let MainView: any
let Props: any

const MainPage: React.FC<RouteComponentProps> = (props) => {
   
  const [showLoading, setShowLoading] = useState(true);
  const [toggle, setListToggle] = useState(true);
 
  Footer()

  useIonViewWillEnter(() => {
  Props = props
  
  });

  useIonViewDidEnter( () => {

     
     setShowLoading(true)
     categoryList = <IsListLoaded />
     MainView = <LoadMoreData />
     setShowLoading(false)
  
  })

  return (
      <IonPage>
   
        <IonHeader >
      
          <TitleBarComponent {...props} />

        </IonHeader> 

        <IonContent> 

          <BannerSlides />

          <p className={styles.heading} style={{marginTop: '-1vh'}}>{Utils.Categories}</p>

          <div className={styles.categoryBg}> 
            {categoryList}
          </div> 
            
          <IonLoading
            isOpen={showLoading}
            onDidDismiss={() => setShowLoading(false)}
            message={'Please wait..'}
          />

          <IonRow style={{padding: '1vh'}}>
            <IonCol col-7 size="10" style={{height:'4vh', marginTop:'1vh',marginBottom:'1.5vh'}}>
              <p style={{fontSize:'2.5vh', fontFamily:'RobotoBold', marginTop:'-0.5vh'}}>{Utils.recommendation}</p>
            </IonCol>
            <IonCol >
              {toggle ? <IonImg src={listView} style={{height:'3.5vh', width: '3.5vh',margin: 'auto'}}
              onClick={e =>{ MainView = <LoadListView/>; setListToggle(false)}}></IonImg>
              : <IonImg src={gridView} style={{height:'3.5vh', width: '3.5vh',margin: 'auto'}}
              onClick={e =>{ MainView = <LoadMoreData/>; setListToggle(true)}}></IonImg>}
            </IonCol>
          </IonRow>

          {MainView}
         
        </IonContent>
        
        <div >
          {foot}
        </div>
  </IonPage>
  ); 
  };

  function Footer() {
  
  if (FirebaseUser.User) {

     foot = <LoggedIn />
    
  }else{
   
    foot = <NotLoggedIn />
  
  }
  }
  
  function LoadMoreData() {    
   return <JustForYou  />
   
  }

  function LoadListView() {    
    return <JustForYouListView  />
    
   }

  
  function LoggedIn(){
    
   return (
    <IonFooter className={styles.footerLoggedIn}>

    <IonGrid>

    <IonRow style={{ margin:0}} >
    <IonCol col-4 >
   
    <IonButtons  >
    <IonImg style={{height: '7vh', width: '7vh', marginLeft: 'auto', marginRight:'auto' }} src={chat} />
    </IonButtons>
    </IonCol>

    <IonCol col-4  >
   
    <IonButtons >
    <IonImg style={{height: '7vh', width: '15vw', marginLeft: 'auto', marginRight:'auto' }}
     src={deals} onClick={e => Props.history.push('/liveDeals' , {title: "Live Deals"})} />
    </IonButtons>
     
    </IonCol>

    <IonCol col-4  >
    <IonButtons >
    <IonImg style={{height: '7vh', width: '15vw', marginLeft: 'auto', marginRight:'auto' }} src={cart} />
    </IonButtons>
  
    </IonCol>

    </IonRow>

    </IonGrid>
      </IonFooter>
    );
  }

  function IsListLoaded(){
    return (
      <FetchDataFromApi itemList={DataList.result} {...Props}/>
    )
   }


  function NotLoggedIn(){
    return (
      <IonFooter className={styles.footertext}>
        <div style={{backgroundColor: 'grey'}}>
        <p style={{fontSize:'90%', display:'inline-block', textAlign:'center'}}>{Utils.footterMsg}</p>
        {/* <IonButtons style={{fontSize:'90%',background:'transparent', textAlign:'center', display:'inline-block', color:'red'}}>
           {Utils.loginButtonText}
        </IonButtons> */}
        </div>
        
     
     </IonFooter>
   );}
  
  function BannerSlides(){

    const settings = {
      dots: true,
      infinite : false,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 500
    };
    return (
      <Slider {...settings} >
        {DataList.result.slice(0,3).map((item, i) =>{

          return( <div key={i} className={styles.bannerContent} onClick={() => { console.log(item.name)}} >
               
                  <IonImg src={UserPicAlt} className={styles.bannerImg}></IonImg>
            
                  </div>
                )
        })
        }
      </Slider>
    )
  }
   

  export default MainPage;