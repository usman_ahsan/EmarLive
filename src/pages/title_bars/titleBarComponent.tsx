import {IonModal ,IonContent, IonImg, IonToolbar, IonButtons, IonSearchbar, IonRow, IonCol, IonGrid, useIonViewWillLeave} from '@ionic/react';
import React, {useState} from 'react';
import styles from '../../theme/MainPage.module.css'
import home from '../../assets/icons/home.png'
import menu from '../../assets/icons/list-menu.png'
import myEnterAnimation from '../../animations/enteranimation'
import myLeaveAnimation from '../../animations/exitAnimaton'
import SideMenu from '../side_menu/sidemenu'
import styless from '../../theme/sidemenu.module.css'
import arrow from '../../assets/icons/arrow-pointing-to-right.svg'
import { RouteComponentProps } from 'react-router';
import firebaseConfig from '../../firebase_config/ConfigFile';
import AuthenticatedUserSideMenu from '../side_menu/authentedSideMenu';
import Utils from '../../Utils';

const TitleBarComponent: React.FC<RouteComponentProps> = (props) => {
  const [showModal, setShowModal] = useState(false);
  const [isUserAuth , setUserauth] = useState(false)
  const renderRedirect = () => {
  props.history.push('/authpage')
  }
  // let User : any
  useIonViewWillLeave(() => {
  setShowModal(false);
  });

  firebaseConfig.auth().onAuthStateChanged((user) => {
    if(user){
    //  User = user
      setUserauth(true)
    }});

    function LoginButton() {
        return (
          <IonButtons className={styless.socialbuttons} onClick={(e) => renderRedirect()}>
          {Utils.loginButtonText}
         </IonButtons>
        )
    }

  return (
    <div>
        <IonToolbar color="header" className={styles.header}>
           <IonGrid>
             <IonRow className={styles.row}>
              
             <IonCol size="1.3" style={{ padding: 'auto', margin: 'auto'}}>
                <IonButtons onClick={e => props.history.replace('/MainPage')}>
                <IonImg style={{height: 34, width: 34 , top: '25%', bottom: '25%', margin: 'auto'}} src={home} />
                </IonButtons>
            </IonCol>

          <IonCol size="9.5"  >
          <IonSearchbar></IonSearchbar>
          </IonCol>

          <IonCol size="1.1"  >
          <IonButtons onClick={() => setShowModal(true)} style={{ margin:'auto',height:50}}>
          <IonImg style={{height: 22, width: 22 , margin: 'auto'}} src={menu} />
          </IonButtons>
          </IonCol>

         </IonRow>

         </IonGrid>

        </IonToolbar>
        

        <IonContent> 
          <IonModal enterAnimation={myEnterAnimation} backdropDismiss={false}
                leaveAnimation={myLeaveAnimation} isOpen={showModal}>
            
            {(isUserAuth)? <AuthenticatedUserSideMenu {...props} /> : <SideMenu /> }
            {(isUserAuth)? "" : <LoginButton />}
            
           <IonButtons onClick={() => setShowModal(false)}>
           <IonImg style={{height: 35, width: 35 , marginBottom: 5, marginLeft: 5, padding: 2}} src={arrow} />
           </IonButtons>

          </IonModal>
        </IonContent>
        </div>
  ); 
  };

  

  export default TitleBarComponent;
